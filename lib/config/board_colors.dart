import 'package:flutter/material.dart';

final boardColors = [
  Colors.amber,
  Colors.blue,
  Colors.deepOrange,
  Colors.green,
  Colors.indigo,
  Colors.purple,
  Colors.yellow,
  Colors.black45,
  Colors.lime,
];
