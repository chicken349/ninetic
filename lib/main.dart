import 'package:flutter/material.dart';
import 'package:ninetic/pages/board/nine_boards_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        dividerTheme: DividerThemeData(
          color: Colors.purple,
          thickness: 10,
          // space: 10,
          indent: 20,
          endIndent: 20,
        ),
      ),
      home: NineBoardsPage(),
    );
  }
}
