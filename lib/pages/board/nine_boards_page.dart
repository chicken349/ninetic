import 'package:flutter/material.dart';
import 'package:ninetic/config/board_colors.dart';
import 'package:ninetic/models/game/history.dart';
import 'package:ninetic/models/game/game_models.dart';
// import 'package:ninetic/models/game/one_board_scores_old.dart';
import 'package:ninetic/models/game/player_scores.dart';
import 'package:ninetic/pages/board/one_board_page.dart';
import 'package:ninetic/utils/board/grid_transformation_helper.dart';
import 'package:ninetic/widgets/board/board_row_and_column_builder.dart';

class NineBoardsPage extends StatefulWidget {
  static const countMatrix = 3;
  static const boardPadding = 15.0;

  final boardIndexes = List.generate(countMatrix,
      (x) => List.generate(countMatrix, (y) => x + y * countMatrix));

  @override
  _NineBoardsPageState createState() => _NineBoardsPageState();
}

class _NineBoardsPageState extends State<NineBoardsPage> {
  static const countMatrix = NineBoardsPage.countMatrix;
  final playerScores = PlayerScores();
  // static final gridTransformation = GridTransformation.create();

  int _round = 1;
  Player _player = Player.O;
  Map<int, List<List<Player>>> _currentBoardState = Map.from(List.generate(
      countMatrix * countMatrix,
      (_) => List.generate(countMatrix,
          (_) => List.generate(countMatrix, (_) => Player.none))).asMap());
  // bool _isEnd = false;
  late int _playerOScore;
  late int _playerXScore;
  List<History> _history = [];
  List<History> _removedHistory = [];
  int _pressableBoardIndex = -1;
  bool _isFreeMode = true;
  List<bool> _boardIsFull =
      List.generate(countMatrix * countMatrix, (_) => false);
  List<int> _boardPlayerTapCount =
      List.generate(countMatrix * countMatrix, (_) => 0);

  void _addHistory(
      {required int boardIndex,
      required Player player,
      required int x,
      required int y,
      required bool isFree,
      int? secondX,
      int? secondY}) {
    if (secondX == null || secondY == null) {
      print(playerScores.getBoardWinningGrids(boardIndex: boardIndex)[player]!);
      _history.add(History(
          boardIndex: boardIndex,
          player: player,
          x: x,
          y: y,
          isFree: isFree,
          score: playerScores.getPlayerCurrentScore(player: player),
          winningGrids: [
            ...playerScores.getBoardWinningGrids(
                boardIndex: boardIndex)[player]!
          ]));
      // print(_scores);
    } else {
      _history.add(History(
          boardIndex: boardIndex,
          player: player,
          x: x,
          y: y,
          isFree: isFree,
          secondX: secondX,
          secondY: secondY,
          score: playerScores.getPlayerCurrentScore(player: player),
          winningGrids: [
            ...playerScores.getBoardWinningGrids(
                boardIndex: boardIndex)[player]!
          ]));
    }
  }

  void _updateScore(
      {required int boardIndex,
      required Player player,
      required List<List<Map<BoardGrid, int>>> winningGrids}) {
    // print(_scores[boardIndex][player]?['col']);
    final scoreToAdd = winningGrids.length;
    // print(winningGrids);

    playerScores.adjustWinningGrids(
      boardIndex: boardIndex,
      grids: winningGrids,
      player: player,
      scoreToAdjust: scoreToAdd,
    );
    // if (player == Player.O) {
    //   _playerOScore += scoreToAdd;
    // } else {
    //   _playerXScore += scoreToAdd;
    // }

    // OneBoardScoresOld? currentScores = _scores[boardIndex][player];
    // // print(currentScores);
    // if (currentScores != null && player == _player) {
    //   _scores[boardIndex][player] = OneBoardScoresOld(
    //       col: currentScores.col + col,
    //       row: currentScores.row + row,
    //       diag: currentScores.diag + diag,
    //       rdiag: currentScores.rdiag + rdiag);
    // }
    // print(_scores);
  }

  // void addValueToMap<K, V>(Map<K, List<V>> map, K key, V value) =>
  //     map.update(key, (list) => list..add(value), ifAbsent: () => [value]);

  void _updateCurrentBoardState({
    required List<List<Player>> board,
    required int key,
  }) {
    _currentBoardState.update(key, (_) {
      return board;
    }, ifAbsent: () {
      return board;
    });
  }

  void _handleTap(
      {required int index,
      required List<List<Player>> board,
      required int x,
      required int y,
      required bool isFull,
      required int playerTapCount}) {
    final nextBoardIndex = GridTransformation.xyToBoardIndex(x: x, y: y);

    // if (_isEnd) return;

    setState(() {
      if (isFull) {
        _boardIsFull[index] = true;
      }

      _round = _round + 1;
      _isFreeMode = _round == 1 || _boardIsFull[nextBoardIndex];

      _pressableBoardIndex = nextBoardIndex;

      // print('setState' + _player.toString());
      _updateCurrentBoardState(board: board, key: index);
      // print(_currentBoardState[index]?[x][y]);

      _calculateScore(boardIndex: index, x: x, y: y);
      // print(_scores);

      if (_removedHistory.length > 0) {
        _removedHistory.clear();
      }

      if (isFull) {
        final boardIndexX = GridTransformation.boardIndexToX(index);
        final boardIndexY = GridTransformation.boardIndexToY(index);

        _calculateScore(
            boardIndex: index,
            x: boardIndexX,
            y: boardIndexY,
            tileDeleteX: x,
            tileDeleteY: y);

        _addHistory(
            boardIndex: index,
            player: _player,
            x: x,
            y: y,
            secondX: boardIndexX,
            secondY: boardIndexY,
            isFree: _isFreeMode);
      } else {
        _addHistory(
            boardIndex: index,
            player: _player,
            x: x,
            y: y,
            isFree: _isFreeMode);
      }

      _player = _player == Player.O ? Player.X : Player.O;
      _boardPlayerTapCount[index] = playerTapCount;

      checkIsEnd();
    });
  }

  // Check out logic here: https://stackoverflow.com/a/1058804
  void _calculateScore(
      {required int x,
      required int y,
      required int boardIndex,
      int? tileDeleteX,
      int? tileDeleteY}) {
    int col = 0, row = 0, diag = 0, rdiag = 0;
    List<int> diagDiff = [];
    List<int> rdiagDiff = [];
    final player = _currentBoardState[boardIndex]?[x][y];
    Map<int, List<List<Player?>>> boardState = _currentBoardState;
    final List<List<Map<BoardGrid, int>>> winningGrids = [];

    if (tileDeleteX != null && tileDeleteY != null) {
      boardState = {
        boardIndex: List.generate(
            countMatrix,
            (xIndex) => List.generate(countMatrix,
                (yIndex) => _currentBoardState[boardIndex]?[xIndex][yIndex]))
      };
      boardState[boardIndex]?[tileDeleteX][tileDeleteY] = Player.none;
    }

    for (int i = 0; i < countMatrix; i++) {
      if (boardState[boardIndex]?[x][i] == player) {
        // print('called col $x $i');
        col++;
      }
      if (boardState[boardIndex]?[i][y] == player) {
        // print('called row $i $y');
        row++;
      }
      if (boardState[boardIndex]?[(x + i + countMatrix) % countMatrix]
              [(y + i + countMatrix) % countMatrix] ==
          player) {
        diagDiff.add((x + i + countMatrix) % countMatrix -
            (y + i + countMatrix) % countMatrix);
        if (diagDiff.every((element) => element == diagDiff[0])) {
          // print('called diag $i $x $y');
          diag++;
        }
      }
      if (boardState[boardIndex]?[(x + i + countMatrix) % countMatrix]
              [(y - i + countMatrix) % countMatrix] ==
          player) {
        rdiagDiff.add((x + i + countMatrix) % countMatrix +
            (y - i + countMatrix) % countMatrix);
        if (rdiagDiff.every((element) => element == rdiagDiff[0])) {
          // print('called rdiag $i $x $y');
          rdiag++;
        }
      }
    }

    if (col == countMatrix) {
      winningGrids.add(List.generate(
          countMatrix, (index) => {BoardGrid.x: x, BoardGrid.y: index}));
    }

    if (row == countMatrix) {
      winningGrids.add(List.generate(
          countMatrix, (index) => {BoardGrid.x: index, BoardGrid.y: y}));
    }

    if (diag == countMatrix) {
      winningGrids.add(List.generate(
          countMatrix,
          (index) => {
                BoardGrid.x: (x + index + countMatrix) % countMatrix,
                BoardGrid.y: (y + index + countMatrix) % countMatrix
              }));
    }

    if (rdiag == countMatrix) {
      winningGrids.add(List.generate(
          countMatrix,
          (index) => {
                BoardGrid.x: (x + index + countMatrix) % countMatrix,
                BoardGrid.y: (y - index + countMatrix) % countMatrix
              }));
    }

    if (winningGrids.length > 0) {
      _updateScore(
          boardIndex: boardIndex, player: player!, winningGrids: winningGrids);
    }
  }

  void removeLastRound() {
    if (_history.length == 0) return;

    final lastRoundDetail = _history.removeLast();
    print('last' + lastRoundDetail.toString());
    // print('last' + lastRoundDetail.toString());
    // print(_round);

    final boardIndex = lastRoundDetail.boardIndex;
    final isFull = _boardIsFull[boardIndex];
    final secondLastRoundDetail = _history.length >= 1 ? _history.last : null;
    final thirdLastRoundDetail =
        _history.length >= 2 ? _history[_history.length - 2] : null;
    // print('secondlast' + secondLastRoundDetail.toString());
    _removedHistory.add(lastRoundDetail);

    if (secondLastRoundDetail != null) {
      // print('called');
      setState(() {
        _pressableBoardIndex = lastRoundDetail.boardIndex;

        _currentBoardState.update(boardIndex, (oneBoardDetail) {
          if (isFull) {
            final boardIndexX = GridTransformation.boardIndexToX(boardIndex);
            final boardIndexY = GridTransformation.boardIndexToY(boardIndex);
            oneBoardDetail[boardIndexX][boardIndexY] = Player.none;
            _boardIsFull[boardIndex] = false;
            // print(_boardIsFull[boardIndex]);
          }
          oneBoardDetail[lastRoundDetail.x][lastRoundDetail.y] = Player.none;
          return oneBoardDetail;
        });

        if (thirdLastRoundDetail != null) {
          removeLastRoundScore(
            boardIndex: boardIndex,
            thirdLastRoundDetail: thirdLastRoundDetail,
            lastRoundDetail: lastRoundDetail,
          );
        }
      });
    } else {
      setState(() {
        _isFreeMode = true;

        _currentBoardState.update(boardIndex, (oneBoardDetail) {
          oneBoardDetail[lastRoundDetail.x][lastRoundDetail.y] = Player.none;
          return oneBoardDetail;
        });
      });
    }

    setState(() {
      _player = _player == Player.O ? Player.X : Player.O;
      _round = _round - 1;
      _isFreeMode = _round == 1 ||
          (secondLastRoundDetail != null && secondLastRoundDetail.isFree);
      _boardPlayerTapCount[boardIndex]--;
      // print(_resumeLastRound);
    });
  }

  void removeLastRoundScore({
    required int boardIndex,
    required History thirdLastRoundDetail,
    required History lastRoundDetail,
  }) {
    // print(thirdLastRoundDetail.scores);
    final _currentPlayer = thirdLastRoundDetail.player;
    // need to check History player...
    // print(lastRoundDetail.scores.sum());
    // print(thirdLastRoundDetail.scores.sum());
    // if (_currentPlayer == Player.O) {
    //   _playerOScore = thirdLastRoundDetail.scores[_currentPlayer]!.length;
    // } else {
    //   _playerXScore = thirdLastRoundDetail.scores[_currentPlayer]!.length;
    // }
    final lastRoundScore = lastRoundDetail.score;
    final thirdLastRoundScore = thirdLastRoundDetail.score;
    // print(lastRoundDetail.scores);
    // print(thirdLastRoundDetail.scores);
    if (lastRoundScore > thirdLastRoundScore) {
      playerScores.adjustWinningGrids(
        boardIndex: boardIndex,
        player: _currentPlayer,
        scoreToAdjust: thirdLastRoundScore - lastRoundScore,
      );
    }
    // print(playerScores.getBoardWinningGrids(boardIndex: boardIndex));
  }

  void recoverLastRound() {
    // print('1' + _removedHistory.toString());
    if (_removedHistory.length == 0) return;

    final lastRoundDetail = _removedHistory.removeLast();
    print('recover' + lastRoundDetail.toString());

    final boardIndex = lastRoundDetail.boardIndex;
    setState(() {
      _history.add(lastRoundDetail);
      // print(_history);

      _pressableBoardIndex = GridTransformation.xyToBoardIndex(
          x: lastRoundDetail.x, y: lastRoundDetail.y);

      _currentBoardState.update(boardIndex, (oneBoardDetail) {
        oneBoardDetail[lastRoundDetail.x][lastRoundDetail.y] =
            lastRoundDetail.player;
        final isFull =
            lastRoundDetail.secondX != null && lastRoundDetail.secondY != null;
        // print(isFull);
        if (isFull) {
          final boardIndexX = GridTransformation.boardIndexToX(boardIndex);
          final boardIndexY = GridTransformation.boardIndexToY(boardIndex);
          oneBoardDetail[boardIndexX][boardIndexY] = lastRoundDetail.player;
          _boardIsFull[boardIndex] = true;
        }
        return oneBoardDetail;
      });

      recoverLastRoundScore(
        boardIndex: boardIndex,
        lastRoundDetail: lastRoundDetail,
      );
    });

    setState(() {
      _player = _player == Player.O ? Player.X : Player.O;
      _round = _round + 1;
      _isFreeMode = _round == 1 || lastRoundDetail.isFree;

      // print(_round);

      // print(lastRoundDetail.isFree);
      _boardPlayerTapCount[boardIndex]++;
      // print(_resumeLastRound);
    });
  }

  void recoverLastRoundScore({
    required int boardIndex,
    required History lastRoundDetail,
  }) {
    // print(thirdLastRoundDetail.scores);
    // _scores[boardIndex][lastRoundDetail.player] = lastRoundDetail.scores;
    final _currentPlayer = lastRoundDetail.player;
    final currentScore =
        playerScores.getPlayerCurrentScore(player: _currentPlayer);
    final currentWinningGridsListLength = playerScores
        .getBoardWinningGrids(boardIndex: boardIndex)[_currentPlayer]!
        .length;

    final scoreToAdjust = lastRoundDetail.score - currentScore;
    // print(scoreToAdjust);
    final winningGridsList = lastRoundDetail.winningGrids;
    // print(winningGridsList);

    // need to check History player...
    // print(lastRoundDetail.scores.sum());
    // print(thirdLastRoundDetail.scores.sum());
    if (scoreToAdjust > 0 && winningGridsList.length > 0) {
      playerScores.adjustWinningGrids(
        boardIndex: boardIndex,
        player: _currentPlayer,
        scoreToAdjust: scoreToAdjust,
        grids: List.generate(
            winningGridsList.length - currentWinningGridsListLength,
            (index) => winningGridsList[index + currentWinningGridsListLength]),
      );
    }
  }

  void checkIsEnd() {
    if (_boardIsFull.every((element) => element)) {
      // if (_history.length >= 2) {
      final scoreDiff = _playerOScore - _playerXScore;
      // _isEnd = true;
      showEndDialog(scoreDiff == 0
          ? 'Draw (O: $_playerOScore, X: $_playerXScore)'
          : 'Player ${scoreDiff > 0 ? Player.O.toShortString() + '(' + _playerOScore.toString() + ')' : Player.X.toShortString() + '(' + _playerXScore.toString() + ')'} wins');
    }
  }

  // @override
  // void initState() {
  //   super.initState();
  // }

  @override
  Widget build(BuildContext context) {
    _playerOScore = playerScores.getPlayerCurrentScore(player: Player.O);
    _playerXScore = playerScores.getPlayerCurrentScore(player: Player.X);
    return Scaffold(
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.all(30),
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        // fixedSize: Size.square(size),
                        // primary: widget.color,
                        ),
                    onPressed: _history.length > 0
                        ? () => setState(() {
                              removeLastRound();
                            })
                        : null,
                    child: Text('Last move'),
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(30),
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      // fixedSize: Size.square(size),
                      primary: Colors.green[400],
                    ),
                    onPressed: _removedHistory.length > 0
                        ? () => setState(() {
                              recoverLastRound();
                            })
                        : null,
                    child: Text('Recover move'),
                  ),
                )
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: BoardRowAndColumnBuilder.modelBuilder(
                  widget.boardIndexes, (x, model) => buildColumn(x)),
            ),
            Container(
                margin: EdgeInsets.only(top: 30),
                child: Column(
                  children: [
                    Text('Current Player: ${_player.toShortString()}',
                        style: TextStyle(fontSize: 18)),
                    Text('Player O: ' + _playerOScore.toString(),
                        style: TextStyle(fontSize: 18)),
                    Text('Player X: ' + _playerXScore.toString(),
                        style: TextStyle(fontSize: 18)),
                  ],
                ))
          ],
        ),
      ),
    );
  }

  Widget buildColumn(int x) {
    // print('nine $x');

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: BoardRowAndColumnBuilder.modelBuilder(
        widget.boardIndexes[x],
        (y, value) => buildField(x, y),
      ),
    );
  }

  Widget buildField(int x, int y) {
    final index = widget.boardIndexes[x][y];
    // print(_boardPlayerTapCount);
    // print(_currentBoardState);
    return Container(
      // margin: const EdgeInsets.all(15.0),
      padding: const EdgeInsets.all(NineBoardsPage.boardPadding),
      decoration: BoxDecoration(border: Border.all(color: Colors.blueAccent)),
      child: OneBoardPage(
        key: ValueKey(index),
        boardIndex: index,
        player: _player,
        onUpdateRound: _handleTap,
        color: boardColors[index],
        isFreeMode: _isFreeMode,
        pressableBoardIndex: _pressableBoardIndex,
        boardState: _round == 1 ? _currentBoardState[index]! : null,
        playerTapCount: _boardPlayerTapCount[index],
      ),
    );
  }

  Future showEndDialog(String title) => showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) => AlertDialog(
          title: Text(title),
          content: Text('Press to Restart the Game'),
          actions: [
            ElevatedButton(
              style: ElevatedButton.styleFrom(primary: Colors.orangeAccent),
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text(
                'Continue',
                textAlign: TextAlign.end,
              ),
            ),
            ElevatedButton(
              onPressed: () {
                // setEmptyFields();
                // Navigator.of(context).pop();
                Navigator.push(
                  context,
                  PageRouteBuilder(
                    pageBuilder: (_, __, ___) => NineBoardsPage(),
                    transitionDuration: Duration(seconds: 0),
                  ),
                );
              },
              child: Text(
                'Restart',
                textAlign: TextAlign.end,
              ),
            ),
          ],
        ),
      );
}
