import 'package:flutter/material.dart';
import 'package:ninetic/models/game/game_models.dart';
import 'package:ninetic/pages/board/nine_boards_page.dart';
import 'package:ninetic/utils/board/grid_transformation_helper.dart';
import 'package:ninetic/widgets/board/board_row_and_column_builder.dart';
import 'dart:math';

class OneBoardPage extends StatefulWidget {
  OneBoardPage({
    required Key key,
    required this.boardIndex,
    required this.player,
    required this.onUpdateRound,
    required this.color,
    required this.isFreeMode,
    required this.pressableBoardIndex,
    required this.boardState,
    required this.playerTapCount,
  }) : super(key: key);

  final int boardIndex;
  final Player player;
  final void Function({
    required int index,
    required List<List<Player>> board,
    required int x,
    required int y,
    required bool isFull,
    required int playerTapCount,
  }) onUpdateRound;
  final Color color;
  final bool isFreeMode;
  final int pressableBoardIndex;
  final List<List<Player>>? boardState;
  final int playerTapCount;

  @override
  _OneBoardPageState createState() => _OneBoardPageState();
}

class _OneBoardPageState extends State<OneBoardPage> {
  static const countMatrix = NineBoardsPage.countMatrix;
  // static final gridTransformation = GridTransformation.create();

  late double size;
  late int _playerTapCount;

  // String lastMove = Player.none;
  late List<List<Player>> _board;

  @override
  void didChangeDependencies() {
    // print('called');
    size = min(
        30,
        min(
            (MediaQuery.of(context).size.width -
                        NineBoardsPage.boardPadding * countMatrix) /
                    (countMatrix * countMatrix) -
                10,
            (MediaQuery.of(context).size.height -
                    200 -
                    NineBoardsPage.boardPadding * countMatrix) /
                (countMatrix * countMatrix)));
    _board = widget.boardState ?? [[]];
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    // print(_playerTapCount);
    _playerTapCount = widget.playerTapCount;

    // print('one' + _board.toString());
    return Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: BoardRowAndColumnBuilder.modelBuilder(
            _board, (x, model) => buildColumn(x)),
      ),
    );
  }

  Widget buildColumn(int x) {
    // print(x);
    final values = _board[x];

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: BoardRowAndColumnBuilder.modelBuilder(
        values,
        (y, value) => buildField(x, y),
      ),
    );
  }

  Widget buildField(int x, int y) {
    // print(y);

    final _sign = _board[x][y].toShortString();
    final _boardIndexFromXY = GridTransformation.xyToBoardIndex(x: x, y: y);
    final isPressableBoard = widget.boardIndex == widget.pressableBoardIndex;
    final isPressableButton = widget.isFreeMode || isPressableBoard;

    void _updateBoardState({required int x, required int y}) {
      setState(() {
        if (_board[x][y] == Player.none) {
          _playerTapCount++;

          // _board = ({0: _board}).update(0, (board) {
          // print(playerTapCount.toString() + 'sss');
          // print(countMatrix * countMatrix - 1);
          _board[x][y] = widget.player;
          bool _isFull = _playerTapCount >= countMatrix * countMatrix - 1;
          if (_isFull) {
            final lastTileX =
                GridTransformation.boardIndexToX(widget.boardIndex);
            final lastTileY =
                GridTransformation.boardIndexToY(widget.boardIndex);

            _board[lastTileX][lastTileY] = widget.player;
          }
          widget.onUpdateRound(
              index: widget.boardIndex,
              board: _board,
              x: x,
              y: y,
              isFull: _isFull,
              playerTapCount: _playerTapCount);

          // return _board;
          // });
        }
      });
    }

    return Container(
      height: size,
      width: size,
      margin: const EdgeInsets.all(1),
      // decoration: BoxDecoration(shape: ),
      child: widget.boardIndex == _boardIndexFromXY && _sign == ''
          ? Container()
          : ElevatedButton(
              style: ElevatedButton.styleFrom(
                // fixedSize: Size.square(size),
                primary: widget.color,
                // elevation: isPressableButton ? 50 : 0
                padding: EdgeInsets.all(0),
              ),
              onPressed: isPressableButton
                  ? () => _updateBoardState(
                        x: x,
                        y: y,
                      )
                  : null,
              child: Text(
                _sign,
                style: TextStyle(fontSize: 20),
                textAlign: TextAlign.center,
              ),
            ),
    );
  }
}
