import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

class BoardSharedPreferences {
  static saveToSharedPreference(
      {required Object object,
      required String key,
      required SharedPreferences pref}) async {
    // final SharedPreferences pref = await SharedPreferences.getInstance();
    // Map json = jsonDecode(jsonString);
    String data = jsonEncode(object);
    await pref.setString(key, data);
  }

  static getSharedPreference(
      {required String key, required SharedPreferences pref}) {
    final jsonString = pref.getString(key);
    if (jsonString != null) {
      return jsonDecode(jsonString);
    }
    return null;
  }
}
