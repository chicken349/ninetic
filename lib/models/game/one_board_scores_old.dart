class OneBoardScoresOld {
  final int col, row, diag, rdiag;

  OneBoardScoresOld({
    required this.col,
    required this.row,
    required this.diag,
    required this.rdiag,
  });

  OneBoardScoresOld.empty()
      : this(
          col: 0,
          row: 0,
          diag: 0,
          rdiag: 0,
        );

  Map<String, int> toMap() =>
      {'col': col, 'row': row, 'diag': diag, 'rdiag': rdiag};

  int getByKey(String propertyName) {
    print('');
    var _mapRep = toMap();
    if (_mapRep.containsKey(propertyName)) {
      return _mapRep[propertyName]!;
    }
    return -10000;
  }

  int sum() {
    return col + row + diag + rdiag;
  }

  @override
  String toString() {
    return {'col': col, 'row': row, 'diag': diag, 'rdiag': rdiag}.toString();
  }
}
