import 'package:ninetic/models/game/game_models.dart';

class History {
  final int boardIndex;
  final Player player;
  final int x, y;
  final bool isFree;
  final int score;
  final List<List<Map<BoardGrid, int>>> winningGrids;
  int? secondX;
  int? secondY;

  History(
      {required this.boardIndex,
      required this.player,
      required this.x,
      required this.y,
      required this.isFree,
      required this.score,
      required this.winningGrids,
      this.secondX,
      this.secondY});

  @override
  String toString() {
    return {
      'boardIndex': boardIndex,
      'player': player,
      'x': x,
      'y': y,
      'isFree': isFree,
      'scores': score,
      'winningGrids': winningGrids,
      'secondX': secondX,
      'secondY': secondY
    }.toString();
  }
}
