import 'package:ninetic/models/game/game_models.dart';
import 'package:ninetic/pages/board/nine_boards_page.dart';

class PlayerScores {
  static const countMatrix = NineBoardsPage.countMatrix;
  static final x = BoardGrid.x, y = BoardGrid.y;
  static final Player _playerX = Player.X, _playerO = Player.O;

  final List<Map<Player, List<List<Map<BoardGrid, int>>>>> _scores =
      List.generate(
          countMatrix * countMatrix, (_) => {Player.X: [], Player.O: []});

  final Map<Player, int> _playerScore = {_playerX: 0, _playerO: 0};

  PlayerScores._privateConstructor();

  static final PlayerScores _instance = PlayerScores._privateConstructor();

  // factory constructor
  factory PlayerScores() {
    return _instance;
  }

  int getPlayerCurrentScore({required Player player}) {
    return _playerScore[player] ?? -1000000;
  }

  void _adjustScore({required int score, required Player player}) {
    _playerScore.update(player, (value) => value + score);
    // return _playerScore;
  }

  void adjustWinningGrids({
    required int boardIndex,
    List<List<Map<BoardGrid, int>>>? grids,
    required Player player,
    required int scoreToAdjust,
  }) {
    if (scoreToAdjust >= 0 && grids != null) {
      _scores[boardIndex][player]!.addAll(grids);
      _adjustScore(score: scoreToAdjust, player: player);
    } else {
      final listLength = _scores[boardIndex][player]!.length;
      _scores[boardIndex][player]!
          .removeRange(listLength + scoreToAdjust, listLength);
      _adjustScore(score: scoreToAdjust, player: player);
    }
  }

  Map<Player, List<List<Map<BoardGrid, int>>>> getBoardWinningGrids(
      {required int boardIndex}) {
    return _scores[boardIndex];
  }

  @override
  String toString() {
    return _playerScore.toString() + '\n' + _scores.toString();
  }
}
