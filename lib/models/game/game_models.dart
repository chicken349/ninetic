enum Player { none, X, O }
enum BoardGrid { x, y }

extension ParseToString on Player {
  String toShortString() {
    switch (this) {
      case Player.none:
        return '';
      case Player.X:
        return 'X';
      case Player.O:
        return 'O';
    }
  }
}

// class Singleton {
//   static final Singleton _instance = Singleton._internal();

//   factory Singleton() => _instance;

//   Singleton._internal();
// }
// // Whenever you need to get the singleton, call its factory constructor, e.g.:
// //   var singleton = Singleton();
// //
// // You'll always get the same instance.

// class Customer {
//   String name;
//   int age;
//   String location;

//   Customer(this.name, this.age, this.location);

//   static final Customer origin = Customer("", 0, "");

//   // factory constructor
//   factory Customer.create() {
//     return origin;
//   }

//   // Redirecting constructors
//   Customer.empty() : this("", 0, "");
//   Customer.withoutLocation(String name, int age) : this(name, age, "");
// }
