import 'package:ninetic/pages/board/nine_boards_page.dart';

class GridTransformation {
  // GridTransformation();

  // static final GridTransformation origin = GridTransformation();

  // factory GridTransformation.create() {
  //   return origin;
  // }
  static const countMatrix = NineBoardsPage.countMatrix;

  static int xyToBoardIndex({required int x, required int y}) {
    return x + y * countMatrix;
  }

  static int boardIndexToX(int boardIndex) {
    return boardIndex % countMatrix;
  }

  static int boardIndexToY(int boardIndex) {
    return boardIndex ~/ countMatrix;
  }
}
